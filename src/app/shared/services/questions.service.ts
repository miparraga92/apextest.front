import {EventEmitter, Injectable} from '@angular/core';
import {QuestionsResource} from '../repositories/questions.resource';
import {IQuestionList} from '../models/question-list.model';

@Injectable({
  providedIn: 'root',
})
export class QuestionsService {
  public onUpdate = new EventEmitter();

  constructor(private questionsResource: QuestionsResource) {
  }

  extract(): Promise<IQuestionList> {
    return this.questionsResource.extract();
  }

  getByJson(): Promise<IQuestionList> {
    return this.questionsResource.getByJson();
  }

  getByMongo(): Promise<IQuestionList> {
    return this.questionsResource.getByMongo();
  }

  updateAll(): Promise<void> {
    return this.questionsResource.updateAll();
  }

  updateJson(): Promise<void> {
    return this.questionsResource.updateJson();
  }


  updateMongo(): Promise<void> {
    return this.questionsResource.updateMongo();
  }
}
