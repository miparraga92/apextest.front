import {HttpClient} from '@angular/common/http';
import {ResourceHandlerHttpClient} from '@ngx-resource/handler-ngx-http';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {IResourceRequest, IResourceResponse} from '@ngx-resource/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

export class TestResourceHandler extends ResourceHandlerHttpClient {

  @BlockUI() block: NgBlockUI;

  constructor(http: HttpClient) {
    super(http);
  }

  handleResponse(req: IResourceRequest, response: HttpResponse<any> | HttpErrorResponse): IResourceResponse {
    if (!response.ok) {
      this.block.reset();
      // window.location.href = '500';
      // return;
    }

    return super.handleResponse(req, response);
  }

}
