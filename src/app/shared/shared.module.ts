import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ResourceModule} from '@ngx-resource/core';
import {HttpClientModule} from '@angular/common/http';
import {QuestionsResource} from './repositories/questions.resource';
import {QuestionsService} from './services/questions.service';
import {FlexLayoutModule} from '@angular/flex-layout';

@NgModule({
  declarations: [],
  imports: [
    HttpClientModule,
    ResourceModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ]
})
export class SharedModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        QuestionsResource,
        QuestionsService
      ]
    };
  }
}
