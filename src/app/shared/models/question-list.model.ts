import {IQuestion} from './question.model';

export interface IQuestionList {
  result: IQuestion[];
  updated: string;
}
