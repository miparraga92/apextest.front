export interface IQuestion {
  index: string;
  value: string;
}
