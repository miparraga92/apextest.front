import {Injectable} from '@angular/core';
import {
  IResourceMethodPromise,
  Resource,
  ResourceAction,
  ResourceActionReturnType,
  ResourceHandler,
  ResourceParams,
  ResourceRequestMethod,
  ResourceResponseBodyType
} from '@ngx-resource/core';
import {environment} from '../../../environments/environment';
import {IQuestionList} from '../models/question-list.model';

@Injectable()
@ResourceParams({
  returnAs: ResourceActionReturnType.Promise
})
export class QuestionsResource extends Resource {

  constructor(handler: ResourceHandler) {
    super(handler);
    this.$setPathPrefix(environment.apiUrl + '/frequent-questions');
  }

  @ResourceAction({
    path: '/extract',
    method: ResourceRequestMethod.Get,
    responseBodyType: ResourceResponseBodyType.Json
  })
  extract: IResourceMethodPromise<null, IQuestionList>;

  @ResourceAction({
    path: '/mongo',
    method: ResourceRequestMethod.Get,
    responseBodyType: ResourceResponseBodyType.Json
  })
  getByMongo: IResourceMethodPromise<null, IQuestionList>;

  @ResourceAction({
    path: '/json',
    method: ResourceRequestMethod.Get,
    // responseBodyType: ResourceResponseBodyType.Json
  })
  getByJson: IResourceMethodPromise<null, IQuestionList>;

  @ResourceAction({
    path: '/update',
    method: ResourceRequestMethod.Put
  })
  updateAll: IResourceMethodPromise<null, null>;

  @ResourceAction({
    path: '/mongo',
    method: ResourceRequestMethod.Put
  })
  updateMongo: IResourceMethodPromise<null, null>;

  @ResourceAction({
    path: '/json',
    method: ResourceRequestMethod.Put
  })
  updateJson: IResourceMethodPromise<null, null>;
}
