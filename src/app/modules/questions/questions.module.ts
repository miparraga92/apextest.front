import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BlockUIModule} from 'ng-block-ui';
import { QuestionsComponent } from './pages/questions/questions.component';
import { HeaderComponent } from './components/header/header.component';
import { QuestionListComponent } from './components/question-list/question-list.component';
import {QuestionsRoutingModule} from './questions-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatExpansionModule} from '@angular/material/expansion';
import {QuestionsService} from '../../shared/services/questions.service';
import {SimplebarAngularModule} from 'simplebar-angular';
import {MatTooltipModule} from '@angular/material/tooltip';

@NgModule({
  declarations: [HeaderComponent, QuestionsComponent, QuestionListComponent],
  imports: [
    CommonModule,
    QuestionsRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    BlockUIModule,
    MatTooltipModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatButtonModule,
    MatMenuModule,
    MatExpansionModule,
    MatSelectModule,
    MatIconModule,
    MatToolbarModule,
    SimplebarAngularModule
  ],
  providers: [
    QuestionsService
  ]
})
export class QuestionsModule {
}

