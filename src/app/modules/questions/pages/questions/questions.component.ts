import {Component, OnInit} from '@angular/core';
import {OriginType} from '../../../../shared/enums/origin.enum';
import {ActivatedRoute} from '@angular/router';
import {IQuestionList} from '../../../../shared/models/question-list.model';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit {

  public currentOrigin: OriginType;
  public currentQuestionList: IQuestionList;

  constructor(private route: ActivatedRoute) {
    this.currentOrigin = route.snapshot.params.type || OriginType.JSON;
  }

  ngOnInit(): void {
  }
}
