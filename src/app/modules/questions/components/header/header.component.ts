import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {OriginType} from '../../../../shared/enums/origin.enum';
import {QuestionsService} from '../../../../shared/services/questions.service';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {IQuestionList} from '../../../../shared/models/question-list.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  @Input() currentOrigin: OriginType;
  @Input() currentQuestionList: IQuestionList;
  @Output() currentOriginChange = new EventEmitter();
  @BlockUI() block: NgBlockUI;

  originType = OriginType;

  constructor(private questionsService: QuestionsService) {
  }

  changeOrigin(origin: OriginType) {
    if (this.currentOrigin !== origin) {
      this.currentOrigin = origin;
      this.currentOriginChange.emit(this.currentOrigin);
    }
  }

  updateList(): void {
    if (this.currentOrigin != null) {
      switch (+this.currentOrigin) {
        case OriginType.JSON:
          this.updateByJson();
          break;
        case OriginType.MongoDB:
          this.updateByMongo();
          break;
      }
    }
  }

  updateByJson(): void {
    this.block.start('Actualizando...');
    this.questionsService.updateJson().then(s => {
      this.questionsService.onUpdate.emit();
    }).catch(e => {
      console.error(e);
    }).finally(() => {
      this.block.stop();
    });
  }

  updateByMongo(): void {
    this.block.start('Actualizando...');
    this.questionsService.updateMongo().then(s => {
      this.questionsService.onUpdate.emit();
    }).catch(e => {
      console.error(e);
    }).finally(() => {
      this.block.stop();
    });
  }
}
