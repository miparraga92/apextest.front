import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {OriginType} from '../../../../shared/enums/origin.enum';
import {QuestionsService} from '../../../../shared/services/questions.service';
import {IQuestionList} from '../../../../shared/models/question-list.model';
import {BlockUI, NgBlockUI} from 'ng-block-ui';

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.css']
})
export class QuestionListComponent implements OnChanges {
  @Input() currentOrigin: OriginType;
  @Input() currentQuestionList: IQuestionList;
  @Output() currentQuestionListChange = new EventEmitter();
  @BlockUI() block: NgBlockUI;

  constructor(private questionsService: QuestionsService) {
    this.questionsService.onUpdate.subscribe(() => {
      this.loadList();
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.currentOrigin) {
      this.loadList();
    }
  }

  loadList(): void {
    if (this.currentOrigin != null) {
      switch (+this.currentOrigin) {
        case OriginType.JSON:
          this.loadByJson();
          break;
        case OriginType.MongoDB:
          this.loadByMongo();
          break;
      }
    }
  }

  loadByJson(): void {
    this.block.start('Cargando...');
    this.questionsService.getByJson().then(s => {
      this.currentQuestionList = s;
      this.currentQuestionListChange.emit(this.currentQuestionList);
    }).catch(e => {
      console.error(e);
    }).finally(() => {
      this.block.stop();
    });
  }

  loadByMongo(): void {
    this.block.start('Cargando...');
    this.questionsService.getByMongo().then(s => {
      this.currentQuestionList = s;
      this.currentQuestionListChange.emit(this.currentQuestionList);
    }).catch(e => {
      console.error(e);
    }).finally(() => {
      this.block.stop();
    });
  }
}
