import { BrowserModule } from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {ResourceHandler, ResourceModule} from '@ngx-resource/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TestResourceHandler} from './shared/handlers/test-resource-handler';
import {SharedModule} from './shared/shared.module';
import {BlockUIModule} from 'ng-block-ui';
import localeAR from '@angular/common/locales/es-AR';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {registerLocaleData} from '@angular/common';
import {SimplebarAngularModule} from 'simplebar-angular';

registerLocaleData(localeAR);
export function myHandlerFactory(http: HttpClient) {
  return new TestResourceHandler(http);
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SimplebarAngularModule,
    BlockUIModule.forRoot(),
    ResourceModule.forRoot({
      handler: {provide: ResourceHandler, useFactory: (myHandlerFactory), deps: [HttpClient]}
    }),
    SharedModule.forRoot()
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'es-AR'},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
