# Apex Test FrontEnd

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.1.

## Run project

Para ejecutar el proyecto debe seguir y ejecutar los siguientes comandos:

* Correr el Apex Test API.

* Ejecutar `npm install`.

* Ejecutar `ng serve --ssl -o`.


El mismo se ejecutara en https://localhost:4200
